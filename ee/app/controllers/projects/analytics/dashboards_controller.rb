# frozen_string_literal: true

module Projects
  module Analytics
    class DashboardsController < Projects::ApplicationController
      include ProductAnalyticsTracking

      feature_category :product_analytics_data_management

      before_action :dashboards_enabled!, only: [:index]
      before_action :authorize_read_product_analytics!
      before_action :authorize_read_combined_project_analytics_dashboards!
      before_action do
        push_frontend_feature_flag(:ai_impact_analytics_dashboard, project.group, type: :gitlab_com_derisk)
        push_frontend_feature_flag(:enable_vsd_visual_editor, project.group)
      end

      before_action :track_usage, only: [:index], if: :viewing_single_dashboard?

      def index; end

      private

      def dashboards_enabled!
        render_404 unless ::Feature.enabled?(:combined_analytics_dashboards, project) &&
          project.licensed_feature_available?(:combined_project_analytics_dashboards) &&
          !project.personal?
      end

      def viewing_single_dashboard?
        params[:vueroute].present?
      end

      def track_usage
        Gitlab::InternalEvents.track_event(
          'analytics_dashboard_viewed',
          project: project,
          user: current_user
        )
      end
    end
  end
end
